.PHONY: elixir go js python


elixir:
	@elixir -r elixir/example.ex -e "Main.run"

go:
	@go run ./go/main.go

ts:
	@tsc js/example.ts
	@node js/example.js

python:
	@python python/main.py

adapter:
	@tsc patterns/adapter/adapter.ts
	@node  patterns/adapter/adapter.js

decorator:
	@tsc patterns/decorator/decorator.ts
	@node  patterns/decorator/decorator.js

facade:
	@tsc patterns/facade/facade.ts
	@node  patterns/facade/facade.js

all:
	@echo "ELIXIR"
	@make elixir
	@echo

	@echo "GO"
	@make go
	@echo

	@echo "TS"
	@make ts
	@echo

	@echo "PYTHON"
	@make python
	@echo

	@echo "PATTERN ADAPTER"
	@make adapter
	@echo

	@echo "PATTERN DECORATOR"
	@make decorator
	@echo
