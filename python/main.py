from abc import ABCMeta, abstractmethod

class Payment():
    __metaclass__=ABCMeta

    @abstractmethod
    def create(arg: str):
        pass


class PaymentA(Payment):
    def create(self, arg):
        return arg
    
class PaymentB(Payment): 
    def create(self, arg):
        return arg

 
d: Payment = PaymentA()

res = d.create("abc")
print(res)

d: Payment = PaymentB()

res = d.create("abc")
print(res)

