// Задача 
// при оформлении заказа показывать уведомление в браузере
// при оплате отправлять sms уведомлление

interface Notificator {
    send(msg: string): string;
}

class SmsNotification implements Notificator{
    send(msg: string): string {return `send SMS notification: ${msg}`}
}
class WebNotification implements Notificator{
    send(msg: string): string {return `send WEB notification: ${msg}`}
}

class Order {
    createOrder(items: string[]): {items: string[], succesful: boolean} 
    {
        return {items: items, succesful: items.length != 0}
    }
}

class Payment {
    payment(amount: number): {amount: number, succesful: boolean}
    {
        return {amount: amount, succesful: amount != 0}
    }
}

class NotificationAdapter {
    send_order(order: {items: string[], succesful: boolean}, notificator: Notificator ): string {
        var msg = `Ваш заказ "${order.items.reduce((pre, cur) => pre + `, ${cur}`)}". ${order.succesful ? "Успешно создан": "Не создан"}.`

        return (new WebNotification()).send(msg)
    }

    send_payment(payment: {amount: number, succesful: boolean}, notificator: Notificator ): string {
        var msg = `Оплата ${payment.amount} руб. ${payment.succesful ? "Прошла успешно": "Не прошла"}.`

        return (new SmsNotification()).send(msg)
    }
}

var notificator : Notificator = new SmsNotification();
var adapter = new NotificationAdapter();


var order = (new Order()).createOrder(["один банан", "два апельсина", "три мандарина"])
var notification1 = adapter.send_order(order, notificator)

console.log()
console.log(notification1)
console.log()


var payment = (new Payment()).payment(120)
var notification2 = adapter.send_payment(payment, notificator)

console.log()
console.log(notification2)
console.log()