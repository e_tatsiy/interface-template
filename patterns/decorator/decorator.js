// Задача
// Отправить пользователю оповещения 
// всеми вариантами, выбранными в настройках
var Notify = /** @class */ (function () {
    function Notify() {
        this.wraps = [];
    }
    Notify.prototype.send = function (msg) {
        return this.wraps.map(function (value) { return "send over ".concat(value, ": ").concat(msg); }).join("\n") + '\n';
    };
    ;
    return Notify;
}());
var SmsNotificationDecorator = /** @class */ (function () {
    function SmsNotificationDecorator() {
    }
    SmsNotificationDecorator.prototype.wrap = function (notification) { notification.wraps.push("SMS"); };
    return SmsNotificationDecorator;
}());
var WebNotificationDecorator = /** @class */ (function () {
    function WebNotificationDecorator() {
    }
    WebNotificationDecorator.prototype.wrap = function (notification) { notification.wraps.push("WEB"); };
    return WebNotificationDecorator;
}());
var TelegramNotificationDecorator = /** @class */ (function () {
    function TelegramNotificationDecorator() {
    }
    TelegramNotificationDecorator.prototype.wrap = function (notification) { notification.wraps.push("Telegram"); };
    return TelegramNotificationDecorator;
}());
var notification = new Notify();
var sendSMS = true;
var sendWEB = true;
var sendTG = true;
if (sendSMS) {
    (new SmsNotificationDecorator()).wrap(notification);
}
if (sendWEB) {
    (new WebNotificationDecorator()).wrap(notification);
}
if (sendTG) {
    (new TelegramNotificationDecorator()).wrap(notification);
}
console.log(notification.send("your notification"));
