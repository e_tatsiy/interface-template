defmodule Payment do
  @callback create(arg :: String.t()) :: String.t()
end

defmodule PaymentA do
  @behaviour Payment

  @impl true
  def create(arg), do: arg

end

defmodule PaymentB do
  @behaviour Payment

  @impl true
  def create(arg), do: arg

end

defmodule Main do

  @payment_a PaymentA
  @payment_b PaymentB

  def run do
    IO.inspect("A")
    @payment_a.create("a create") |> IO.inspect()

    IO.inspect("B")
    @payment_b.create("b create") |> IO.inspect()
  end

end
