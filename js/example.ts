interface Payment {
    create(arg: string): string;
}
 
class PaymentA implements Payment{
    create(arg: string): string {return arg}
}

class PaymentB implements Payment{
    create(arg: string): string {return arg}
}

let d : Payment = new PaymentA;
console.log(d.create("abc"))

d = new PaymentB;
console.log(d.create("abc"))