// Задача
// Отправить пользователю оповещения 
// всеми вариантами, выбранными в настройках

class Notify {
    wraps: string[] = [];
    send(msg: string): string {
        return  this.wraps.map((value) => `send over ${value}: ${msg}`).join("\n") + '\n'
     };
}


interface NotificatorDecorator {
    wrap(notification: Notify);
}


class SmsNotificationDecorator implements NotificatorDecorator{
    wrap(notification: Notify) {notification.wraps.push("SMS")}
}
class WebNotificationDecorator implements NotificatorDecorator{
    wrap(notification: Notify) {notification.wraps.push("WEB")}
}
class TelegramNotificationDecorator implements NotificatorDecorator{
    wrap(notification: Notify) {notification.wraps.push("Telegram")}
}


var notification = new Notify();


var sendSMS = true
var sendWEB = true
var sendTG = true

if (sendSMS){
    (new SmsNotificationDecorator()).wrap(notification)
}

if (sendWEB){
    (new WebNotificationDecorator()).wrap(notification)
}

if (sendTG){
    (new TelegramNotificationDecorator()).wrap(notification)
}

console.log(notification.send("your notification"))