package main

import "fmt"

type Payment interface {
	create(arg string) string
}

type PaymentA struct{
}

func (d PaymentA) create(arg string) string {
	return arg
}

type PaymentB struct{
}

func (d PaymentB) create(arg string) string {
	return arg
}

func main(){
	var d Payment = PaymentA{}
	
	fmt.Println("A")
	fmt.Println(d.create("a create"))

	d = PaymentB{}

	fmt.Println("B")
	fmt.Println(d.create("a create"))
}