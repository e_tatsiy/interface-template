// Задача 
// при оформлении заказа показывать уведомление в браузере
// при оплате отправлять sms уведомлление
var SmsNotification = /** @class */ (function () {
    function SmsNotification() {
    }
    SmsNotification.prototype.send = function (msg) { return "send SMS notification: ".concat(msg); };
    return SmsNotification;
}());
var WebNotification = /** @class */ (function () {
    function WebNotification() {
    }
    WebNotification.prototype.send = function (msg) { return "send WEB notification: ".concat(msg); };
    return WebNotification;
}());
var Order = /** @class */ (function () {
    function Order() {
    }
    Order.prototype.createOrder = function (items) {
        return { items: items, succesful: items.length != 0 };
    };
    return Order;
}());
var Payment = /** @class */ (function () {
    function Payment() {
    }
    Payment.prototype.payment = function (amount) {
        return { amount: amount, succesful: amount != 0 };
    };
    return Payment;
}());
var NotificationAdapter = /** @class */ (function () {
    function NotificationAdapter() {
    }
    NotificationAdapter.prototype.send_order = function (order, notificator) {
        var msg = "\u0412\u0430\u0448 \u0437\u0430\u043A\u0430\u0437 \"".concat(order.items.reduce(function (pre, cur) { return pre + ", ".concat(cur); }), "\". ").concat(order.succesful ? "Успешно создан" : "Не создан", ".");
        return (new WebNotification()).send(msg);
    };
    NotificationAdapter.prototype.send_payment = function (payment, notificator) {
        var msg = "\u041E\u043F\u043B\u0430\u0442\u0430 ".concat(payment.amount, " \u0440\u0443\u0431. ").concat(payment.succesful ? "Прошла успешно" : "Не прошла", ".");
        return (new SmsNotification()).send(msg);
    };
    return NotificationAdapter;
}());
var notificator = new SmsNotification();
var adapter = new NotificationAdapter();
var order = (new Order()).createOrder(["один банан", "два апельсина", "три мандарина"]);
var notification1 = adapter.send_order(order, notificator);
console.log();
console.log(notification1);
console.log();
var payment = (new Payment()).payment(120);
var notification2 = adapter.send_payment(payment, notificator);
console.log();
console.log(notification2);
console.log();
